import io

from flask import Flask, make_response, send_file
from flask import render_template, request
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import pickle
import utils
import base64
import glob
from zipfile import ZipFile


app = Flask(__name__)
stm = utils.load_csv("/app/tridas/static/STM/metadata.csv")
poc_stm = utils.load_csv("/app/tridas/static/STM/metadata.csv")
poc_stm['n_labels'] = poc_stm.Label.astype('category').cat.codes
path = "/app/tridas/static/STM/data/"


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/dashboard_stm/', methods=['GET', 'POST'])
def show_dashboard():
    total = len(stm)
    x_ax = request.form.get('X')
    y_ax = request.form.get('Y')
    plots = []
    if x_ax == y_ax:
        y_ax = None
    axis = utils.get_axis(x_ax, y_ax)
    if not axis:
        return render_template('dashboard_stm.html', plots=plots)
    metadata = utils.get_chosen_metadata(poc_stm, axis)
    plot = utils.bokeh_plot(metadata, total)
    plots.append(plot)
    return render_template('dashboard_stm.html', plots=plots)


@app.route('/dashboard_stm/images/', methods=['GET', 'POST'])
def show_metadata_table():
    x = request.args.get('x')
    y = request.args.get('y')
    xval = request.args.get('xval')
    yval = request.args.get('yval')
    cols = [x, y]
    values = [xval, yval]
    imgs = utils.filter_df_columns(poc_stm, cols, values).sort_values('Date')
    return render_template(
        'stm_images.html',
        tables=[imgs.to_html(table_id="STM",
                             index=False, bold_rows=False,
                             classes='table table-bordered table-striped',
                             header="true")])


@app.route('/dashboard_stm/show_image/', methods=['GET', 'POST'])
def show_image():
    img_id = request.args.get('ID')
    row, img = utils.get_img_by_id(poc_stm, path, int(img_id))
    fig = Figure(figsize=(8, 8))
    axis = fig.add_subplot(1, 1, 1)
    date = row.Date
    fname = row.TF0_Filename
    img.plot(ax=axis, cmap='afmhot', add_colorbar=False)
    axis.set_title("{}".format(img_id), fontsize=16)
    canvas = FigureCanvas(fig)
    output = io.BytesIO()
    canvas.print_png(output)
    image = base64.b64encode(output.getvalue()).decode('utf8')
    return render_template('image.html', image=image, img_id=img_id, description=row)


@app.route('/dashboard_stm/download/', methods=['GET', 'POST'])
def download_data_files():
   img_id = request.args.get('ID')
   data_files =  glob.glob(f'{path}/{img_id}.*')
   metadata = io.StringIO()
   img_df = poc_stm[poc_stm['ID'] == int(img_id) ]
   img_df.to_csv(metadata, index=False)
   metadata.seek(0)
   data = io.BytesIO()
   prov = '/app/tridas/static/STM/provenance.json'
   data_files.append(prov)
   with ZipFile(data, mode='w') as z:
       for f in data_files:
           f_name = f.split('/')[-1]
           z.write(f, f_name)
       z.writestr(f'{img_id}.csv', metadata.getvalue())
   data.seek(0)
   return send_file(
        data,
        mimetype='application/zip',
        as_attachment=True,
        attachment_filename=f'{img_id}.zip'
    )

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
