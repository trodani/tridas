FROM python:3.8

RUN apt-get clean \
    && apt-get -y update

RUN apt-get -y install build-essential

WORKDIR /app

RUN git clone https://gitlab.com/trodani/tridas.git
RUN pip install -r tridas/requirements.txt --src /usr/local/src
RUN wget https://zenodo.org/record/5808724/files/STM.zip -O /app/tridas/static/STM.zip
RUN unzip /app/tridas/static/STM.zip -d /app/tridas/static/
RUN wget https://zenodo.org/record/5808724/files/provenance.json -O /app/tridas/static/STM/provenance.json

EXPOSE 8080

CMD [ "python", "/app/tridas/app.py" ]
